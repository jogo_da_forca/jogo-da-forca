//package br.unifil.dc.lab2;

import javax.swing.JPanel;

import java.util.Random;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * Write a description of class ListaGravada here.
 * 
 * @author Natália Kaneshime e Rebeca Ito 
 * @version 21/04/2020
 */
public class ListaGravada implements Transparencia
{
    /**
     * Constructor for objects of class ListaGravada
     */
    public ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }
    
    public void pintar(Graphics2D pincel, JPanel contexto) {
        Dimension dim = contexto.getSize();
        
        StringBuilder txtDim = new StringBuilder("Dimensões da tela: ");
        txtDim.append(dim.width).append(" X ").append(dim.height);
        
        StringBuilder txtDesenho = new StringBuilder("Pintura: ");
        pincel.drawString(txtDim.toString(), TAMANHO_MARGEM_TXT, TAMANHO_LINHA_TXT);
        txtDesenho.append(lista);
        pincel.drawString(txtDesenho.toString(), TAMANHO_MARGEM_TXT, TAMANHO_LINHA_TXT * 2);
        acharMaiorElementos(lista);
        compararElementos(lista, maiorElemento);
    }
 
    private void compararElementos(List<Integer> valores, int maiorElemento){
        listaProporcoes = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++){
            float listTemp = lista.get(i);
            float temp = listTemp / maiorElemento;
            listaProporcoes.add(i, temp);
        }
    }

    private void acharMaiorElementos(List<Integer> lista) {
        for(int i = 0; i < lista.size(); i++){
            if (lista.get(i) > maiorElemento){
                maiorElemento = lista.get(i);
            }
        }
    }

    public static  Color getRandomColor() {
        Random random = new Random();
        int red = random.nextInt(256);
        int blue = random.nextInt(256);
        int green = random.nextInt(256);
        return new Color(red, green, blue);
    }

    public static void colorPesquisa(Graphics2D pincel){
        for(int i = 0; i < lista.size(); i++){
            if(i == 0 || i == lista.size()){
                pincel.setColor(Color.RED);
            }
        }
    }
    
    public static List<Integer> lista;
    private List<Color> coresIndices;
    private String nome;
    private List<Float> listaProporcoes;
    private int maiorElemento;
    private final int TAMANHO_MARGEM_TXT = 10;
    private final int TAMANHO_LINHA_TXT = 15;
}