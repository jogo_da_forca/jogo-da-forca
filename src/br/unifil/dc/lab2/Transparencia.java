//package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.Graphics2D;

public interface Transparencia{
    void pintar(Graphics2D g2d, JPanel contexto);
}
